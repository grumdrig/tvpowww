TV Powww!
=========

Does anybody remember [TV Powww](http://en.wikipedia.org/wiki/TV_Powww)?
Kids would call in to a cartoon show and play a horrible video game on
their (and everyone's) TV by yelling "Pow!" into their telephone. Now
you can relive that experience [in the privacy of your own
browser](https://tv-powww.appspot.com/).

This has been tested with Google Chrome only, and probably won't work
with other browsers. It's in HTML and Javascript.

Controls
--------

Shout "Pow!" to start the game. Then shout "Pow!" to fire at the enemy
saucers. Just shout "Pow!" no matter what. You can shout other things
too -- no, actually, don't do that. Just "Pow!" and nothing else.
Don't worry about grandma downstairs, or the other people in the
library or in the next cubicle, or what they might think. They'll
simply assume you're on crack, which was the prevailing theory anyway.
Belt it out, loud and proud. "Pow!"

Links
-----

* [Play it!](https://tv-powww.appspot.com/)

* [Video of original intro](http://www.youtube.com/watch?v=08JwB2XQ3zM)
  (Isn't "The Only Game in Town!" a propitious tagline?)

* [Video of original gameplay](http://www.youtube.com/watch?v=PNjyARmcg2Q)

* [The getUserMedia API](http://www.html5rocks.com/en/tutorials/getusermedia/intro/)

* [SDTV standard](http://en.wikipedia.org/wiki/Standard-definition_television)
